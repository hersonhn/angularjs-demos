## AngularJS demos


- Reddit reader: <http://hersonhn.github.io/angularjs-demos/reddit>

- Note taking app: <https://github.com/HersonHN/angularjs-demos/tree/master/notes> (To test this demo you should use the [raw-rest-server](https://github.com/HersonHN/raw-rest-server))

