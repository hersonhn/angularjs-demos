'use strict';
////// App
var app = angular.module('reddit', ['ui.state', 'ngResource']);
var darkPanel = document.getElementById('dark-panel');

app.config(function ($stateProvider, $urlRouterProvider, $rootScopeProvider) {
    var views = {
        subreddits: { controller: 'SubredditsController', templateUrl: 'templates/subreddits.html' },
        posts:      { controller: 'PostsController',      templateUrl: 'templates/posts.html' },
        singlepost: { controller: 'SinglePostController', templateUrl: 'templates/singlepost.html' }
    };

    $stateProvider.state('subreddit', { views: views, url: '/:subreddit' });
    $stateProvider.state('post',      { views: views, url: '/:subreddit/:post' });
    $urlRouterProvider.otherwise( '/gifs' );

    $rootScopeProvider.digestTtl(111);
});


app.factory('Subreddit', function($resource) {
    var Subreddit = $resource(
        'http://www.reddit.com/r/:subreddit.json',
        { subreddit: '@subreddit' },
        { query: { method: 'JSONP', isArray: false } }
    );

    Subreddit.sanitize = function (response) {
        if ( !(response.kind == 'Listing') || !(response.data.children) ) {
            throw new Error('Wrong response');
        }
        return response.data.children.map(postSanitizer);
    };

    return Subreddit;
});


app.factory('Post', function($resource) {
    var Post = $resource(
        'http://www.reddit.com/comments/:post.json',
         { post: '@post' },
         { query: { method: 'JSONP', isArray: false } }
    );

    Post.sanitize = function(response) {
        var post = postSanitizer(response[0].data.children[0]);
        var comments = response[1].data.children.map(commentSanitizer);
    
        return {
            post: post,
            comments: comments
        };
    }

    return Post;
});


app.service('shared', function () {
    var data = {};
    return {
        get: function (key, fallback)  {
            return (data[key] || fallback);
        },
        set: function (key, value) {
            return data[key] = value;
        }
    }
});


app.filter('unencode', function() {
    return function(html) {
        var textarea, value;
        textarea = document.createElement('textarea');
        textarea.innerHTML = html || '';

        value = textarea.value;
        value = value.replace(/<a href="\//g, '<a href="http://www.reddit.com/');
        value = value.replace(/<a /g, '<a target="_blank" ');
        return value;
    }
});


app.controller('SubredditsController', function ($scope, $stateParams) {
    $scope.active = $stateParams.subreddit;
    $scope.subreddits = 'gifs pics funny news IAmA'.split(' ');
});


app.controller('PostsController', function ($scope, Subreddit, $stateParams, shared) {
    $scope.entries = [];

    var subreddit = $stateParams.subreddit;
    var post = $stateParams.post;
    $scope.activePost = post;
    $scope.subreddit = subreddit;
    $scope.title     = subreddit;

    $scope.listPosts = function (subreddit) {

        $scope.title = subreddit;
        $scope.entries = [];

        if (subreddit) {
            $scope.loading = true;
        }

        var cachedPosts = shared.get('posts:' + subreddit, {});

        if (cachedPosts.length) {
            $scope.entries = cachedPosts;
            $scope.loading = false;

            restoreScrollPosition(shared);
            return;
        }

        if (subreddit)

        get(Subreddit, { subreddit: subreddit }, function (response) {
            if (!response) return;
            
            restoreScrollPosition(shared);

            response = Subreddit.sanitize(response);
            $scope.entries = response;
            $scope.loading = false;

            shared.set('posts:' + subreddit, response);
        });
    };

    $scope.showPost = function () {
        $scope.saveScrollPosition();
    };

    $scope.saveScrollPosition = function () {
        shared.set('scroll-top', darkPanel.scrollTop);
    };

    $scope.listPosts(subreddit);
});


app.controller('SinglePostController', function ($scope, shared, $stateParams, Post) {
    var subreddit = $stateParams.subreddit;
    var post      = $stateParams.post;

    $scope.activePost = post;
    $scope.loading = !!$scope.activePost;

    if (post)

    get(Post, { post: post }, function (response) {
        if (!response) return;
        response = Post.sanitize(response);

        $scope.post     = response.post;
        $scope.comments = response.comments;
        $scope.loading  = true;

        showPost($scope.post);
    });

    function showPost(entry) {
        var type;

        $scope.activePost = entry.id;
        $scope.loading = false;

        type = getPostType(entry);
        entry.type = {};
        entry.type[type] = true;

        if (type == 'image') {
            setImage(entry.url);
        }

        $scope.post = entry;
    }

    function getPostType(entry) {
        var url, isImage, isExternal, isPost, isLink;

        url = entry.url || '';
        isImage = !!url.match(/\.(jpg|gif|bmp|jpeg|png)$/);
        isPost  = !!entry.html;
        isLink  = !!entry.url;

        if (isImage) return 'image';
        if (isPost)  return 'post';
        if (isLink)  return 'external';

        return '404';
    }

    // you can't update the image src while it's loading because it won't
    // (blame the browser)
    function setImage(url) {
        var div = document.getElementById('post-image');
        div.innerHTML = '';

        setTimeout(function () {
            div.innerHTML = '';
            
            var img = document.createElement('img');
            img.src = url;
            div.appendChild(img);
        }, 200);
    }
});


function get(model, parameters, cb) {
    cb = cb || function () {};

    // creating the temporal callback function
    get.count = get.count || 1;
    var x = get.count++;
    var callback_name = 'internalcallback' + x;
    
    window[callback_name] = function (data) {
        cb(data);
        delete window[callback_name];
    }
    parameters.jsonp = callback_name;
    model.query(parameters);
}


function postSanitizer(child) {
    var row = {};
    
    row.title     = child.data.title || '';
    row.id        = child.data.id || '';
    row.url       = child.data.url || '';
    row.permalink = child.data.permalink || '';
    row.html      = child.data.selftext_html || '';
    row.nsfw      = !!child.data.over_18;

    return row;
}


function commentSanitizer(child) {
    var comment = {};
    var replies = [];
    
    comment.id     = child.data.id || '';
    comment.text   = child.data.body || '';
    comment.html   = child.data.body_html || '';
    comment.author = child.data.author || '';
    comment.downs  = child.data.downs || '';
    comment.ups    = child.data.ups || '';

    if (child.data.replies &&
        child.data.replies.data &&
        child.data.replies.data.children &&
        child.data.replies.data.children.length)
    {
        replies = child.data.replies.data.children.map(commentSanitizer);
    }
    comment.replies = replies;
    
    return comment;
}


function restoreScrollPosition(shared) {
    setTimeout(function () {
        darkPanel.scrollTop = shared.get('scroll-top', 0); 
    }, 10);
}


