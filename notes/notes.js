var notes = angular.module('notes', ['ngResource']);

notes.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/config', {
        templateUrl: 'templates/config.html'
    });
});


notes.factory('Notes', function ($resource) {
    return $resource('/api/notes/:id', { id: '@id'}, {
        'query':  { method: 'GET', isArray: false },
        'update': { method: 'PUT' },
        'delete': { method: 'DELETE' },
        'save':   { method: 'POST' }
    });
});

notes.factory('Store', function() {
    var store = window.localStorage || {
        getItem: angular.noop,
        setItem: angular.noop
    };

    return {
        get: function(key, fallback) {
            return JSON.parse(store.getItem(key)) || fallback;
        },
        set: function(key, value) {
            store.setItem(key, JSON.stringify(value));
        }
    }
})

notes.directive('ngKeypress', function() {
    return function(scope, element, attrs) {
        element.bind('keyup', function(event) {
            scope.$apply(function() {
                scope.$eval(attrs.ngKeypress);
            });
        });
    };
});


notes.filter('summary', function () {
    return function (text) {
        text = text || '';
        text = text.trim().split('\n')[0];
        return text;
    }
})


notes.controller('navigation', function ($scope, $rootScope, Notes, $location, $http, Store) {

    var saveDelay = 1000;
    var editor = ace.edit('editorarea');
    var delayedSave = {};

    $scope.activeTag = null;
    $scope.notes = [];
    $scope.active = {};

    $rootScope.configs = Store.get('config') || {
        vim: 'off',
        language: 'markdown',
        theme: 'tomorrow'
    };

    Notes.query(function(results) {
        updateNoteList(results.notes);

        $rootScope.setSettings();
    });


    $scope.showNote = function (note) {
        note = note || {};
        $scope.active = note;
        $scope.tagsString = (note.tags || []).join(', ');
        
        editor.setValue(note.text || '');
    };


    $scope.listNotesWithTag = function (tagName) {
        if (!$scope.notes.length) return false;
        
        if (tagName == null) {
            $scope.notes = $scope.allNotes;
            return;
        }

        $scope.notes = _.chain($scope.allNotes).filter(function (note) {
            return _.indexOf(note.tags, tagName) > -1;
        }).value();
    };


    $scope.newNote = function () {
        $scope.showNote();
    };


    $scope.waitforsave = function () {
        var note, id, text, tags;
        note = $scope.active;
        id = note.$$hashKey;

        // set the text
        text = editor.getSession().getValue();
        note.text = text;

        delayedSave[id] = delayedSave[id] || _.debounce(save, saveDelay);
        delayedSave[id](note, $scope.tagsString);
    }


    $scope.removeNote = function (active) {
        $scope.allNotes = $scope.allNotes.filter(function (row) {
            return row.id != active.id
        });

        if ($scope.notes.length == 1) {
            $scope.activeTag = null;
            $scope.listNotesWithTag(null);
        }
        updateNoteList($scope.allNotes);
        $scope.showNote($scope.notes[0]);

        Notes.remove({ id: active.id });
    };


    $rootScope.showSettings = function () {
        $http.get('ace-avaliable.json').then(function(response) {
            $rootScope.ace = response.data;
            $location.path('/config');
        });
    };


    $rootScope.closeSettings = function () {
        $location.path('/');
    };


    $rootScope.setSettings = function () {
        Store.set('config', $rootScope.configs);

        editor.setTheme('ace/theme/' + $rootScope.configs.theme);
        editor.getSession().setMode('ace/mode/' + $rootScope.configs.language);
        
        if ($rootScope.configs.vim == 'on') {
            editor.setKeyboardHandler(ace.require('ace/keyboard/vim').handler);
        } else {
            editor.setKeyboardHandler(null);
        }
    }


    $rootScope.editorSettings = function () {
        $rootScope.setSettings();
        this.closeSettings();
    };


    // Watchers
    watch(editor, ['onTextInput', 'onCut', 'onPaste'], function() {
        $scope.waitforsave();
    });


    function updateNoteList(notes) {
        $scope.allNotes = notes;
        $scope.notes = notes;
        
        var tags = [ { id: null, name: 'All Notes'} ];

        _.chain(notes).pluck('tags').flatten().unique().sort()
        .forEach(function (value, index) {
            tags.push({ id: value, name: value })
        });
        
        $scope.tags = tags;
    }

    function save(note, tagsString) {
        // set the tags
        tags = (tagsString || '').split(',').map(function(t) { return t.trim(); });
        note.tags = tags;

        if (note.id) {
            Notes.update(note, function (response) {
                angular.copy(response, $scope.active);
                updateNoteList($scope.allNotes)
            });

        } else {
            Notes.save(note, function (response) {
                note.id = response.id;
                $scope.active = note;
                $scope.allNotes.push(note);
                updateNoteList($scope.allNotes)
            });
        }
    }

});


function watch(parent, events, callback) {
    events.forEach(function (e) {
        var original = parent[e];
        
        parent[e] = function () {
            var args = Array.prototype.slice.apply(arguments);
            original.apply(this, args);
            callback(e, args);
        };
    });
}


