var app = angular.module('myapp', ['ngResource']);

app.factory('Locations', function ($resource) {
    return $resource('/api/locations/:id', { id: '@id'}, {
        'query':  { method: 'GET', isArray: false },
        'update': { method: 'PUT' },
        'delete': { method: 'DELETE' }
    });
});

app.controller('Form', function ($scope, $http, Locations) {
    $scope.data = [];
    $scope.countries = [];
    $scope.cities = [];

    $http.get('data.json').then(function(results) {
        $scope.countries = results.data.countries;
        $scope.cities    = results.data.cities;
    });

    $scope.$watch('country', function (new_value) {
        $scope.city = {};
    });

    Locations.query(function (result) {
        $scope.data = result.locations;
    });

    $scope.validate = function () {
        if (!$scope.city.name) {
            $scope.error = 'Select a City.';
            return false;
        }

        if (!$scope.country.name) {
            $scope.error = 'Select a Country.';
            return false;
        }

        if (!$scope.description) {
            $scope.error = 'Enter a Description.';
            return false;
        }

        $scope.error = '';
        return true;
    };

    $scope.add = function () {
        if (!$scope.validate()) return;

        var newValues = {
            'city': $scope.city,
            'country': $scope.country,
            'description': $scope.description
        };

        $scope.data.push(newValues);
        Locations.save(newValues);
        $scope.clean();
    };

    $scope.clean = function () {
        $scope.country = {};
        $scope.city = {};
        $scope.description = '';
    }

    $scope.clearErrors = function () {
        $scope.error = ''
    };
});

