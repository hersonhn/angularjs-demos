var app = angular.module('contacts', ['ngResource']);

app.factory('Contact', function ($resource) {
    return $resource('/api/contacts/:id', { id: '@id'}, {
        'query':  { method: 'GET', isArray: false },
        'update': { method: 'PUT' },
        'delete': { method: 'DELETE' }
    });
});

app.controller('List', function ($scope, $resource, Contact) {
    $scope.neo = {};
    $scope.currently_editing = null;

    $scope.contacts = Contact.query(function (result) {
        $scope.contacts = result.contacts;
    });

    $scope.add = function () {
        var newRegistry = $scope.neo;
        Contact.save(newRegistry, function (savedRegistry) {
            newRegistry.id = savedRegistry.id;
        });
        $scope.contacts.push(newRegistry);
        $scope.neo = {};
    };

    $scope.edit = function ($index) {
        cleanLast();

        $scope.editing = angular.copy($scope.contacts[$index]);
        $scope.currently_editing = $index;
        $scope.contacts[$index].isEditing = true; 
    };

    $scope.save = function ($index) {
        var row = angular.copy($scope.editing);
        $scope.currently_editing = null;
        
        Contact.update(row, function () {
            $scope.contacts[$index] = row;
        });
    };

    $scope.remove = function (index, id) {
        confirm('Are you sure?') &&

        Contact.remove({ 'id': id }, function () {
            $scope.contacts.splice(index, 1);
        });
    };

    function cleanLast() {
        var index = $scope.currently_editing;
        if (!angular.isNumber(index)) return false;
        $scope.contacts[index].isEditing = false;
    }


});

